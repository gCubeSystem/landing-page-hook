
# Changelog for landing-page-hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v7.0.0] - 2020-07-21

Task #19696 LR User Account page should redirect to KC user Account page, this components implements the redirection

Ported to git

## [v1.0.0] - 2016-03-16

First release 
