package org.gcube.portal.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gcube.portal.landingpage.LandingPageManager;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;

public class LandingPageAction extends Action {
	private static Log _log = LogFactoryUtil.getLog(LandingPageAction.class);

	private static final String VHOST_TO_SKIP = "dev.d4science.org";
	@Override
	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		
		_log.info("LandingPageAction v. 2 is ON");
		
		HttpSession session = request.getSession();
		String path = "";
		String currentVirtualHost = "";
		try {
			path = getCustomLandingPage(request);
			currentVirtualHost = request.getServerName();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		if (currentVirtualHost.compareTo(VHOST_TO_SKIP) != 0)
			session.setAttribute(WebKeys.LAST_PATH, new LastPath(StringPool.BLANK, path));
		else {
			_log.debug("Found VHOST_TO_SKIP = " +  currentVirtualHost);
		}
	}
	/**
	 * Returns custom landing page path after user login
	 * 
	 * @param request
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	private String getCustomLandingPage(final HttpServletRequest request) throws PortalException, SystemException {
		String customLandingPagePath ="";
		customLandingPagePath = LandingPageManager.getLandingPagePath(request);
		_log.info("Private Site LandingPage Path = " +  customLandingPagePath);
		return customLandingPagePath;
	}		
}