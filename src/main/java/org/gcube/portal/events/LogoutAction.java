package org.gcube.portal.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.util.PortalUtil;

public class LogoutAction extends Action {
	private static Log _log = LogFactoryUtil.getLog(LogoutAction.class);
	private static final String EDIT_ACCOUNT_PARAMNAME = "edit-account";
	/**
	 * Redirect to the keycloak edit account page if the /c/portal/logout contains such parameter not null, regular logout outherwise
	 */
	@Override
	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		String editAccount = request.getParameter(EDIT_ACCOUNT_PARAMNAME);
		String currentVirtualHost = "";
		try {
			currentVirtualHost = request.getServerName();
			if (editAccount != null) {
				StringBuilder sb = new StringBuilder(PrefsPropsUtil.getString(PortalUtil.getCompanyId(request), "d4science.oidc-user_account_url"));
				sb.append("?referrer=").append(currentVirtualHost);
				_log.debug("Edit account, redirect to " + sb.toString());
				response.sendRedirect(sb.toString());
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} 
	
	}
}